/*
 * Authored by user#4927 in 2019. Feel free to do basically whatever you want
 * with this but please provide attribution. And acknowledge that while I tried
 * my best while writing this, don't hold me liable for bugs or other reasons.
 * Knock yourself out :D
 * 
 * or, if you prefer legalese:
 * 
 * Copyright 2019 user#4927
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

require('dotenv').config()
const ENV = process.env

const exitHook = require('async-exit-hook');
const discord = require('discord.js')
const puppeteer = require('puppeteer')

const client = new discord.Client()

const RYES = '\u2705';
const RNO = '\u274c';
const RDONE = '\u2714';

const admins = new Set(ENV.ADMINS.split(",").filter(x => x.length))
const only_ops = new Set(ENV.OPS.split(",").filter(x => x.length))
const ops = new Set([...admins, ...only_ops])

function target() {
    return client.channels.get(ENV.TARGET_CHANNEL)
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function forever() {
    return new Promise(resolve => {})
}

function contains_any(set, having) {
    for (let s of set) {
        if (having.has(s)) {
            return true
        }
    }
    return false
}

async function processMessageForLevel(name, content) {
    console.log(name + "<>" + content)
    
    let match = content.match(/[A-Z0-9][A-Z0-9][A-Z0-9][ -][A-Z0-9][A-Z0-9][A-Z0-9][ -][A-Z0-9][A-Z0-9][A-Z0-9]/g)
    
    if (match) {
        for (let m of match) {
            let msg = await target().send(m + " by " + name)
            await msg.react(RYES)
            await msg.react(RNO)
        }
    }
}

async function setupPuppeteer() {
    const browser = await puppeteer.launch({headless: false})
    exitHook(async done => {
        await browser.close()
        done()
    });
    const page = await browser.newPage()
    await page.goto(ENV.CHATURL)
    
    await page.evaluate(() => { // open dropdown
        document.querySelector('#view-selector paper-button').click()
    })
    await sleep(1000)
    await page.evaluate(() => { // select Live chat, rather than top chat
        Array.from(document.querySelectorAll('paper-item-body .item')).filter(x => x.innerText.match(/Live chat/))[0].click()
    })
    await sleep(1000)
    
    let lastMsg
    async function processMessages() {
        let messages = await page.$$('yt-live-chat-text-message-renderer')
        try {
            let processing = false;
            for (let m of messages) {
                let mid = await page.evaluate(node => node.id, m)
                if (processing) {
                    // dealing with new messages
                    let innerText = await page.evaluate(node => node.innerText, m)
                    let [name, content] = innerText.split('\n')
                    processMessageForLevel(name, content)
                } else if (mid === lastMsg) {
                    // this is the last message we've seen last time, so any after this message will be new
                    processing = true
                }
            }
            lastMsg = await page.evaluate(node => node.id, messages[messages.length-1])
        } finally {
            for (let m of messages) {
                m.dispose() // free puppeteer element handles to avoid memory leaks
            }
            setTimeout(processMessages, 1000)
        }
    }
        
    setTimeout(processMessages, 1000)
}

client.on('message', async msg => {
    if (ops.has(msg.author.id)) {
        if (msg.content === '!clearqueue') {
            let log = await target().fetchMessages({ limit: 100 });
            for (let m of log.array()) {
                await m.delete()
            }
        }
    }
    if (msg.content === 'ping') {
        msg.reply('pong')
    }
    console.log(msg)
})

client.on('ready', async evt => {
    target().fetchMessages({ limit: 100 })
    
    setupPuppeteer()
})

client.on('messageReactionAdd', async evt => {
    if (contains_any(ops, evt.users)) {
        if (evt.emoji.name === RNO) {
            evt.message.delete()
        } else if (evt.emoji.name === RYES) {
            evt.message.clearReactions()
            evt.message.edit(RDONE + " (done) " + evt.message.content)
        }
    }
})

client.login(ENV.BOT_TOKEN)